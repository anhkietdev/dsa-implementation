# Linked Data Structure

- **Linked Data Structure** is a data organization in which each node includes itself data and links to others.
![img0](./img/0.png)

## Types of Linked Data Structures

1. **Linear data structure / List**

- Organizes data in a single direction.
- Example: Array, List.

2. **Linear Linked data structure / Linked List**

- Data is organized linearly through links between nodes.
- Example: Linked List.

3. **Non Linear Linked data structure**

- Data can be accessed through multiple paths.
- Example: Tree.

## Linked List

- **Linked List**: A collection of nodes, where each node contains its data and links to other nodes.

- **Link**: A link describes a previous-next relation  between two elements.

- **Linear Accessing Path**: Links between nodes form a linear path, allowing access element in a specific order.

- Most common used Linked Lists:
  - `Singly linked lists (SLL)`: Each node has a reference to the next node.
  - `Circular linked lists (CSLL)`: The last link points to the first node, forming a circle.
  - `Doubly linked lists (DLL)`: Each node has references to both the previous and next nodes.
  - `Circular doubly linked lists (CDLL)`: The end and beginning of the list are connected, forming a circle.

![img1](./img/1.png)
![img2](./img/2.png)
