﻿/// <summary>
/// Represents a simple Node with its data (of type int) and a reference to the next node.
/// </summary>
public class Node
{
    #region Fields
    public int Data; // Data stored in the node
    public Node Next; // Reference to the next node in the linked list
    public Node Previous; // Reference to the previous node in the linked list
    #endregion

    #region Constructor
    /// <summary>
    /// Initializes a new instance of the Node class with the given data.
    /// </summary>
    /// <param name="data">The data to be stored in the node.</param>
    public Node(int data)
    {
        Data = data; //Set the data of the node
        Next = null; // Initialize the Next reference to null
        Previous = null; // Initialize the Previous reference to null
    }
    #endregion
}

public class LinkedList
{
    #region Fields
    private Node head;
    private Node tail;
    #endregion

    #region Constructor
    public LinkedList()
    {
        head = null;
        tail = null;
    }
    #endregion

    #region Methods
    /// <summary>
    /// Add a new node with the specified data to the beginning of the linked list.
    /// </summary>
    /// <param name="data">The data to be stored in the new node</param>
    public void AddFirst(int data)
    {
        Node newNode = new Node(data);

        if (this.IsEmpty())
        {
            // If the list is empty, set both head and tail to the new node.
            head = tail = newNode;
        }
        else
        {
            newNode.Next = head; // Add newNode to the beginning.
            newNode.Previous = null; // Because newNode now is the first, so newNode.Previous = null.
            head.Previous = newNode; // Old head was moved by newNode, so (old)head.Previous = newNode.
            head = newNode; // Set head to newNode.
        }
    }

    /// <summary>
    /// Adds a new node with the specified data to the end of the linked list.
    /// </summary>
    /// <param name="data">The data to be stored in the new node.</param>
    public void AddLast(int data)
    {
        Node newNode = new Node(data);

        if (this.IsEmpty())
        {
            // If the list is empty, set both head and tail to the new node.
            head = tail = newNode;
        }
        else
        {
            newNode.Previous = tail; // Add newNode to the last of linked list.
            newNode.Next = null; // Because newNode now is the last, no newNode.Next = null.
            tail.Next = newNode; // New tail was added by newNode, so now updated tail is newNode.
            tail = newNode; // Set tail to newNode
        }
    }

    //TO DO: Add next ref node
    //TODO : Search node
    //TO DO: Traverse
    //TO DO: Sorting





    /// <summary>
    /// Checks if the linked list is empty.
    /// </summary>
    /// <returns>True if the linked list is empty, otherwise false.</returns>
    public bool IsEmpty()
    {
        return head == null;
    }

    /// <summary>
    /// Displays the elements of the linked list.
    /// </summary>
    public void DisplayList()
    {
        Node current = head; // Start from the head of the list

        while (current != null)
        {
            Console.WriteLine($"{current.Data}"); // Display the data of the current node
            current = current.Next; // Move to the next node
        }

        Console.WriteLine(); // Add a newline for better readability
    }
    #endregion

}