﻿
//list.AddFirst(1);
//list.AddFirst(2);
//list.AddFirst(3);

////list.AddLast(7);
////list.AddLast(8);
////list.AddLast(9);

//Console.OutputEncoding = System.Text.Encoding.Default;
//Console.WriteLine("Danh sách liên kết sau khi thêm vào đầu: ");
//list.DisplayList();

Console.WriteLine("*****SINGLY LINKED LIST*****");
int choice = -1;

LinkedList list = new LinkedList();
do
{

    Menu.Display();
    choice = int.Parse(Console.ReadLine());

    switch (choice)
    {
        case 1:
            Console.Write("Enter data to add first: ");
            int dataToAddFirst = int.Parse(Console.ReadLine());
            list.AddFirst(dataToAddFirst);            

            break;

        case 2:
            Console.Write("Enter data to add last: ");
            int dataToAddLast = int.Parse(Console.ReadLine());
            list.AddLast(dataToAddLast);

            break;
        case 3:
            Console.WriteLine("Add new node after 1 node");
            break;

        default:
            break;
    }
} while (choice != 0);

/// <summary>
/// Represents a simple Node with its data (of type int) and a reference to the next node.
/// </summary>
public class Node
{
    #region Fields
    public int Data;  // Data stored in the node
    public Node Next; // Reference to the next node in the linked list
    #endregion

    #region Constructor
    /// <summary>
    /// Initializes a new instance of the Node class with the given data.
    /// </summary>
    /// <param name="data">The data to be stored in the node.</param>
    public Node(int data)
    {
        Data = data;  // Set the data of the node
        Next = null;  // Initialize the Next reference to null
    }
    #endregion
}

/// <summary>
/// Represents a linked list data structure.
/// </summary>
public class LinkedList
{
    #region Fields
    private Node head; // Reference to the first node in the linked list
    private Node tail; // Reference to the last node in the linked list
    #endregion

    #region Constructor
    /// <summary>
    /// Initializes a new instance of the LinkedList class.
    /// </summary>
    public LinkedList()
    {
        head = null; // Initialize the head reference to null (empty list)
        tail = null; // Initialize the tail reference to null (empty list)
    }
    #endregion

    #region Methods
    /// <summary>
    /// Adds a new node with the specified data to the beginning of the linked list.
    /// </summary>
    /// <param name="data">The data to be stored in the new node.</param>
    public void AddFirst(int data)
    {
        Node newNode = new Node(data); // Create a new node with the given data

        if (this.IsEmpty())
        {
            // If the list is empty, set both head and tail to the new node
            head = tail = newNode;
        }
        else
        {
            // If the list is not empty, update the Next reference of the new node and set head to the new node
            newNode.Next = head;
            head = newNode;
        }
    }

    /// <summary>
    /// Adds a new node with the specified data to the end of the linked list.
    /// </summary>
    /// <param name="data">The data to be stored in the new node.</param>
    public void AddLast(int data)
    {
        Node newNode = new Node(data); // Create a new node with the given data

        if (this.IsEmpty())
        {
            // If the list is empty, set both head and tail to the new node
            head = tail = newNode;
        }
        else
        {
            // If the list is not empty, update the Next reference of the current tail and set tail to the new node
            tail.Next = newNode;
            tail = newNode;
        }
    }

    /// <summary>
    /// Adds a new node with the specified data after the reference node in the linked list.
    /// </summary>
    /// <param name="refNode">The reference node after which the new node will be added.</param>
    /// <param name="data">The data to be stored in the new node.</param>
    public void AddAfter(Node refNode, int data)
    {
        if (refNode == null)
        {
            Console.WriteLine("Invalid reference node.");
            return;
        }

        Node newNode = new Node(data);

        // Update the Next reference of the new node and set it after the reference node
        newNode.Next = refNode.Next;
        refNode.Next = newNode;

        // If the reference node is the tail, update the tail to the new node
        if (refNode == tail)
        {
            tail = newNode;
        }
    }

    /// <summary>
    /// Searches for the first occurrence of a specific value in the linked list.
    /// </summary>
    /// <param name="searchedData">The value to search for.</param>
    /// <returns>
    /// The index of the first occurrence of the specified value in the linked list, or -1 if the value is not found.
    /// </returns>
    public int SearchIfExist(int searchedData)
    {
        if (this.IsEmpty())
        {
            Console.WriteLine("Linked list is empty.");
            return -1;
        }

        Node current = head;
        int index = 0;

        // Iterate through the linked list
        while (current != null)
        {
            // Check if the current node's data matches the searched value
            if (current.Data == searchedData)
            {
                Console.WriteLine($"{searchedData} is in the list at index {index}.");
                return index;
            }

            current = current.Next;
            index++;
        }

        Console.WriteLine($"{searchedData} is not found in the list.");
        return -1;
    }

    //TO DO: refactor later
    public void Delete(int value)
    {
        if (IsEmpty())
        {
            return;
        }
        if (head.Data == value)
        {
            head = head.Next;
            return;
        }

        var next = head;
        while (next.Next != null) 
        {
            if (next.Next.Data == value)
            {
                next.Next = next.Next.Next;
                return;
            }
            next = next.Next;
        }
    }

    /// <summary>
    /// Reverses the linked list.
    /// </summary>
    public void Reverse()
    {
        // Check if the linked list is empty or contains only one element
        if (this.IsEmpty() || head == tail)
        {
            return;
        }

        Node current = head; // Pointer to the current node being processed
        Node previous = null; // Pointer to the previous node in the reversed linked list
        Node nextNode; // Pointer to the next node in the original linked list

        // Traverse the original linked list
        while (current != null)
        {
            nextNode = current.Next; // Preserve the link to the next node in the original list
            current.Next = previous; // Reverse the link

            previous = current; // Move the previous pointer to the current node
            current = nextNode; // Move the current pointer to the next node
        }

        // At the end of the loop, 'previous' points to the head of the reversed linked list
        // Update 'head' and 'tail'
        tail = head;
        head = previous;
    }

    /// <summary>
    /// Sorts the linked list in ascending order using the Bubble Sort algorithm.
    /// </summary>
    public void AscendingSort()
    {
        // Check if the linked list is empty or contains only one element
        if (this.IsEmpty() || head == tail)
        {
            return;
        }

        Node i, j;

        // Outer loop (i) runs from the head to the node before the tail
        for (i = head; i != tail; i = i.Next)
        {
            // Inner loop (j) runs from the node after i to the end of the list
            for (j = i.Next; j != null; j = j.Next)
            {
                // Compare data of adjacent nodes (i and j) and swap if needed
                if (j.Data < i.Data)
                {
                    // Swap data between i and j
                    int temp = i.Data;
                    i.Data = j.Data;
                    j.Data = temp;
                }
            }
        }
    }

    /// <summary>
    /// Checks if the linked list is empty.
    /// </summary>
    /// <returns>True if the linked list is empty, otherwise false.</returns>
    public bool IsEmpty()
    {
        return head == null;
    }

    /// <summary>
    /// Displays the elements of the linked list.
    /// </summary>
    public void DisplayList()
    {
        Node current = head; // Start from the head of the list

        while (current != null)
        {
            Console.WriteLine($"{current.Data}"); // Display the data of the current node
            current = current.Next; // Move to the next node
        }

        Console.WriteLine(); // Add a newline for better readability
    }
    #endregion
}

/// <summary>
/// Console menu for your choice.
/// </summary>
public static class Menu
{
    /// <summary>
    /// Display menu with multiple choice.
    /// </summary>
    public static void Display()
    {
        Console.WriteLine("1. Add to first.");
        Console.WriteLine("2. Add to last.");
        Console.WriteLine("3. Add after a node.");
        Console.WriteLine("4. Search a node.");
        Console.WriteLine("5. Reverse a linked list.");
        Console.WriteLine("6. Ascending sort.");
        Console.WriteLine("0. Exit");
        Console.Write("Enter your choice: ");
    }
}
