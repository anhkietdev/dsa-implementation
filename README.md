# DSA Implementation

## Overview

Welcome to my DSA Implementation repository! Here, I share my journey of self-learning and practicing the implementation of Data Structures and Algorithms using C# code.

Feel free to explore, learn, and share!

## Table of Contents

1. [Linked Data Structures](./LinkedLists/)
   - Introduction to linked data structures.
   - Types of linked data structures.
   - Detailed explanation of linked lists.

2. [Stack And Queue](./Stacks-Queues/)

3. [Sorting](./Sorting/)

4. [Recursion](./Recursion/)

5. [Tree](./Tree/)

## Contribution

Feel free to contribute to this repository by adding more implementations, explanations, or improvements. Your contributions are highly appreciated!

## Contact

For any inquiries or contributions, you can reach me at <truonganhkietdev@gmail.com>
